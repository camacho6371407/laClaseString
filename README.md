# POO. FUNDAMENTOS DEL LENGUAJE JAVA
## La classe String
### Yostin Arbey Camacho Velásquez

> En este cocumento se encuentra solucionada la actividad **UF4_Be1_E1**. Los ejercicios del 1 al 6 se muestran con imágenes del su ejecución seguido de una explicación de su desarrollo en este README, pero el código del proyecto estará en la carpeta comprimida **ycamacho_UF4_Be1_E1**. El ejecicio 7 está directamente solucionado en el presente documento. 

## SOLUCIÓN
### Ejercicio 1
![001cod](001_cod.png)  
![001](ej_001.png)  

Se reibe un texto por teclado y este mismo se usa como parámetro para la función *contarChar()*, la cual utilizando *length()* para saber la longitud del texto compara, caracter a caracter con *charAt()* para saber si coincide con el char 'e', si es así la variable *nChar* aumenta su valor en una unidad. Al final se retorna el valor de *nChar*

### Ejercicio 2
![002](ej_002.png)  
Se determina dentro de la variable textoEj la palabra a adivinas, en este caso sería 'cocodrilo'. Luego se utiliza un *for*  dentro del cual se compara con *equals()* si la palabra ingresada por pantalla es igual a la que hay que acertar, si no es el caso vuelve a preguntar (2 veces más, con que que es un total de 3 intentos) hasta que se acierte o se acaben los intentos. 

### Ejercicio 3
![003](ej_003.png)  
Este ejercicio es muy sencillo, simplememte se ultiliza *length()* para saber la longitud, es decir, el número de caracteres del texto.

### Ejercicio 4 y ejercicio 5
![004](ej_004.png)  
![005](ej_005.png)  
Ambos ejercicio son muy parecidos, por ende sus soluciones tambíen. El ejercicio 4 cuenta las vocales de un texto y utiliza la función *contarVocales()*, la cual internamente recorre caracter a caracter y comprueba si el index que se compara es una a, e, i, o, u (con el operador OR ||). Por otro lado el ejercicio 5 cuenta las consonantes y usa la función *contarConsonantes()* e internamete compara si el index no es una a, e, i, o, u o un espacio en blaco (utilizando el operador AND &&).

![cod](004-005_cod.png)  

### Ejercicio 6
![006](ej_006.png)  
Para solucionar este ejercicio primero se recibe un texto por teclado, el cuál se utiliza como parámetro para la función *contarHome()*. Esta función recibe un texto y lo divide en palabras usando como guía los espacios en blanco ( split(" ") ). Cada una de estas palabras se almacenará en un array de Strings llamado 'palabras' el cual se recorre y se compara cada uno de sus índices con *equals()* para saber si es igual a 'home'  
![cod7](007_cod.png)  

### Ejercicio 7

* **indexOf( )**: Retorna el índice de la primera aparicón del parámetro indicado.  
    + indexOf( char b ):  Este método devuelve el índice del carácter 'b' pasado como parámetro. Si ese carácter no está disponible en la cadena, el índice devuelto sería -1.
    + indexOf( char b, int i ): Busca el caracter indicado a partir del índice 'i', si el caracter aparece antes será ignorado.
    + indexOf( String str ):  Devuelve el índice del primer carácter de la subcadena que se le pasa como parámetro. Si la subcadena no se encuentra retorna -1.
    + indexOf( String str, int i): Busca la subcadena indicada a partir del índice 'i', si la subcadena aparece antes será ignorada.
* **isEmpty( )**: Retorna *true* en caso de que la longitud de la cadena sea 0, de lo contrario retorna *false*.  
* **endsWith( )**: Se utliza para comprobar si la cadena termina con la subcadena indicada y retorna *true* en caso verdadero, *false* si no lo hace.
* **compareToIgnoreCase( )**: Se utiliza para comparar dos cadenas ignorando si los carecters son mayúsculas o minúsculas. Puede retornar:
    + 0, esto significa que ambos elementos comparados son iguales.
    + menor que 0, simboliza que el argumento es lexicograficamente mayor que la variable.  
    + mayor que 0, significa que el argumento es lexicograficamente menor que la variable. 
*  **lastIndexOf( )**: Devuelve la posición de la última aparición de un carácter especificado o una subcadena en una cadena:
    + lastIndexOf( char b ): Este método devuelve el índice del carácter 'b' pasado como parámetro. Si ese carácter no está disponible en la cadena, el índice devuelto sería -1.
    + lastIndexOf( char b , int i): Busca recorriendo en reversa el  texto el caracter indicado a partir del índice 'i'.
    + lastIndexOf( String str ): acepta una cadena como argumento y devuelve el índice dentro de esta cadena de la primera aparición de la subcadena especificada. Si no aparece como una subcadena, el método devuelve -1.  
    + lastIndexOf( String str, int i): devuelve el índice dentro de esta cadena de la última aparición de la subcadena especificada, buscando hacia atrás a partir del índice especificado. 
* **replace( ), replaceAll( ), replaceFirst( )**: Estos métodos sirven para reemplazar una parte de una cadena por otra nueva parte indicada.
    + replace( ): Reemplaza todas las apariciones de un **caracter** o **subcadena** por otra indicada. 
    + replaceAll( ): Reemplaza todas las paracines de una **subcadena** por otra indicada.
    + replaceFirst( ): Reemplaza únicamene la primera aparición de un caracter o subcadena.
* **split( )**: Divide una cadena en subcadenas utilizando un delimitador definido
* **substring( int inici, int final )**: Divide una cadena y retorna el contenido entre el índice 'inici' y el índice 'final'. Si no se especifica el íncide final se entenderá que se quiere la subcadena desde 'inici' hasta el final del texto. 
* **toCharArray( ):** Convierte una cadena en un array de caracteres.
* **toLowerCase( )**: Convierte a todos los caracteres de una cadena en minúsculas.
* **toString( )**: Es una función que se hereda de la clase Object, y sirve para tener la representación es String de un objete determinado. 
* **toUpperCase( )**: Convierte a todos los caracteres de una cadena en mayúsculas.
* **startsWith( )**: Se utliza para comprobar si la cadena comienza con la subcadena indicada y retorna *true* en caso verdadero, *false* si no lo hace.
* **trim( )**: Elimina todos los espacios en blanco iniciales y finales de una cadena.
* **valueOf( )**: Convierte a una cadena String los tipos de datos pasados como parámetro.

#### EJEMPLOS
<td></td>
<tr></tr>

> **VARIABLES A UTILIZAR**  
 Estas variables servirán para aplicarles los distintos métodoa vistos anteriormente.

 ```
    public class Run {

        public static void main(String[] args){

            String ej1= "Tres tristes tigres comian trigo en un trigal";
            String ej2= "Por la mañana me gusta tomar café";
            String ej3= "Tierra a la vista";
            String ej4= "";
            String ej5= "   Buenas tardes  ";

            Persona p1= new Persona("Yostin", "Camacho", 19);
        }
    }
 ```


<table border=1>
    <tr><th>MÉTODO</th><th>RESULTADO</th></tr>
    <tr>
        <td>ej1.indexOf( 's' )</td>
        <td>3</td>
    </tr>
    <tr>
        <td>ej1.indexOf( 's', 5 )</td>
        <td>8</td>
    </tr>
    <tr>
        <td>ej1.indexOf( "tigres" )</td>
        <td>5</td>
    </tr>
    <tr>
        <td>ej1.indexOf( "es", 11 )</td>
        <td>17</td>
    </tr>
    <tr>
        <td>ej2.isEmpty( )<br>ej4.isEmpty( )</td>
        <td>false<br>true</td>
    </tr>
    <tr>
        <td>ej3.endsWith("vista")</td>
        <td>true</td>
    </tr>
    <tr>
        <td>ej3.compareToIgnoreCase( "TieRRAa A la viSta" )</td>
        <td>0</td>
    </tr>
    <tr>
        <td>ej3.lastIndexOf( 'c' )<br>ej3.lastIndexOf( 'c', 28 )<br>
        ej3.lastIndexOf( "café" )<br>ej3.lastIndexOf( "Café", 28 )</td>
        <td>29<br>-1<br>29<br>-1</td>
    </tr>
    <tr>
        <td>ej3.replace( 'r', 'g' )<br>ej2.replaceAll( "café", "chocolate")<br>ej3.replaceFirst( 'a','A')</td>
        <td>"Tiegga a la vista"<br>"Por la mañana me gusta tomar chocolate"<br>"TierrA a la vista"</td>
    </tr>
    <tr>
        <td>ej3.stplit(" ")</td>
        <td>["Tierra", "a", "la", "vista"]</td>
    </tr>
    <tr>
        <td>ej1.substring( 0, 4)</td>
        <td>"Tres"</td>
    </tr>
    <tr>
        <td>p1.getNom( ).toCharArray( )</td>
        <td>["Y", "o", "s", "t", "i", "n"]</td>
    </tr>
    <tr>
        <td>p1.getNom( ).toLowerCase( )</td>
        <td>yostin</td>
    </tr>
    <tr>
        <td>p1.toString( )</td>
        <td>"Persona [nom: Yostin, apellido: camacho, edad: 19]"</td>
    </tr>
    <tr>
        <td>p1.getNom( ).toUpperCase( )</td>
        <td>YOSTIN</td>
    </tr>
    <tr>
        <td>ej2.startsWith( "Por" )</td>
        <td>true</td>
    </tr>
    <tr>
        <td>ej5.trim(  )</td>
        <td>"Buenas tardes"</td>
    </tr>
        <tr>
        <td>String.valueOf(384.8)</td>
        <td>"384.8"</td>
    </tr>
</table>

